# _list-books_


## Rationale

I store my e-books as _EPUB_ or _PDF_ files as follows:

In the "start" directory `~/Books`, for each author with more than one file
there exists a subdirectory with the same name as the author (all uppercase),
where all spaces are replaced with underscores.

This subdirectory may contain further subdirectories (for book series) and
_EPUB_ or _PDF_ files as in the following example:

````
JAMES_PATTERSON
├── 2012_Zoo.epub
└── Alex_Cross
    ├── 2012_Dark.epub
    ├── 2013_Run.epub
    └── 2017_Hate.epub
````

If I own only one book from a specific author, the _EPUB_ or _PDF_ file is
directly stored in the "start" directly like so; author and book title are
separated by `_-_`:

````
ADAM_NEVILL_-_Apartment_16.epub
````

The book title names are always set in uppercase and lowercase letters.

I could list these books as follows:

````
ADAM_NEVILL_-_Apartment_16.epub
JAMES_PATTERSON/2012_Zoo.epub
JAMES_PATTERSON/Alex_Cross/2012_Dark.epub
JAMES_PATTERSON/Alex_Cross/2013_Run.epub
JAMES_PATTERSON/Alex_Cross/2017_Hate.epub
````

But I prefer a format, where the all-uppercase letters of the author names are
converted to uppercase and lowercase letters, the underscores are replaced with
blanks, and the file name extensions are removed:

````
* Adam Nevill
    * Apartment 16

* James Patterson
    * 2012 Zoo
    * Alex Cross - 2012 Dark
    * Alex Cross - 2013 Run
    * Alex Cross - 2017 Hate
````


## The `list-books` tool

The tool `list-books` is able to create a book list in the desired format. It is
written in Haskell.


### Compiling and Linting

Since `list-books.hs` does not use "special" modules, the compilation should be
possible with the standard Haskell installation:

````bash
$ sudo apt install haskell-platform hlint
$ ghc --make -Wall -O2 list-books.hs
````

Lint:

````bash
$ hlint -s list-books.hs
````

### "Installation"

Copy `list-books` to a directory that is listed in the environment variable
`PATH`, e.g. `~/bin` (or create a symlink).


### Usage

Call `list-books` this way to get a short usage message:

````bash
$ list-books -h
Usage: list-books [DIR-PATH]
````

If `list-books` is executed without any command line parameter, it uses the
directory `$HOME/Books` as "start" directory, where `HOME` is an environment
variable. If `HOME` is not set, `Books` (in the current working directory) is
used instead.

It is also possible to set the "start" directory explicitly via command line as
absolute or relative path.

Examples:

````bash

$ list-books  # use default '$HOME/Books'
$ list-books rel/path/to/books
$ list-books /abs/path/to/books
````


<sub>[Wolfgang](mailto:haskell@wu6ch.de)</sub>


<!-- EOF -->

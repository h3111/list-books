{-|
  list-books.hs

  Usage:    list-books [DIR-PATH]

  Examples: list-books  # use default DIR-PATH '$HOME/Books'
            list-books rel/path/to/books
            list-books /abs/path/to/books

  Compile:  ghc --make -Wall -O2 list-books.hs
  Lint:     hlint -s list-books.hs

  Author:   haskell@wu6ch.de
-}


import Control.Applicative ((<|>))
import Control.Monad (filterM)
import Control.Monad.State (StateT, execStateT, liftIO, modify)

import Data.Char (toLower, toUpper)
import Data.Bool (bool)
import Data.List ((\\), intercalate, sort)
import Data.List.Split (splitOn)
import Data.Maybe (fromMaybe)

import System.Directory (doesDirectoryExist, listDirectory, setCurrentDirectory)
import System.Environment (getArgs, lookupEnv)
import System.FilePath ((</>), dropExtension)

import qualified Data.Map as Map


{-|
  The 'BookDataMap' type is a map with a String (the author name) as key and
  a list of String (the titles of the related books) as value.
-}
type BookDataMap = Map.Map String [String]


{-|
  The default directory (appended to the value of the environment variable
  'HOME') that is used, if 'DIR-PATH' is not given explicitly via command line.
-}
defaultDir :: FilePath
defaultDir = "Books"


{-|
  In a file path (which represents a book), each string that matches the first
  pair element is replaced with the second pair element. Each pair of the list
  is applied one after the other.
-}
replacements :: [(String, String)]
replacements =
  [ ("_-_",           "/")
  , ("_",             " ")
  , ("AE",            "Ä")
  , ("Ae",            "Ä")
  , ("ae",            "ä")
  , ("OE",            "Ö")
  , ("Oe",            "Ö")
  , ("oe",            "ö")
  , ("UE",            "Ü")
  , ("Ue",            "Ü")
  , ("ue",            "ü")
  , ("Bloss",         "Bloß")
  , ("bloss",         "bloß")
  , ("Grösse",        "Größe")
  , ("gross",         "groß")
  , ("Strasse",       "Straße")
  , ("strasse",       "straße")
  ]


{-|
  In a book entry, as a final correction, each string that matches the first
  pair element is replaced with the second pair element. Each pair of the list
  is applied one after the other.
-}
corrections :: [(String, String)]
corrections =
  [ ("aü",            "aue")
  , ("eü",            "eue")
  , ("Andre ",        "André ")
  , ("Blüs",          "Blues")
  , ("blüs",          "blues")
  , ("Dötsch",        "Doetsch")
  , ("Düll",          "Duell")
  , ("düll",          "duell")
  , ("reqünz",        "requenz")
  , ("georg",         "Georg")
  , ("Goyvärts",      "Goyvaerts")
  , ("Görzen",        "Goerzen")
  , ("Heissel",       "Heißel")
  , ("Josse",         "Jossé")
  , ("Jö",            "Joe")
  , ("jochen",        "Jochen")
  , ("jürgen",        "Jürgen")
  , ("-grün",         "-Grün")
  , ("Lars Jäger",    "Lars Jaeger")
  , ("Manül",         "Manuel")
  , ("Marletta-hart", "Marletta-Hart")
  , ("Mcfadyen",      "McFadyen")
  , ("Mcdevitt",      "McDevitt")
  , ("Michäl",        "Michael")
  , ("O Malley",      "O'Malley")
  , ("O Sullivan",    "O'Sullivan")
  , ("Qüry",          "Query")
  , ("qüry",          "query")
  , ("Qüst",          "Quest")
  , ("qüst",          "quest")
  , ("Remarqü",       "Remarque")
  , ("Riessinger",    "Rießinger")
  , ("bert Klassen",  "bert Klaßen")
  , (" Und ",         " und ")
  , (" Van ",         " van ")
  , ("Von Fournier",  "von Fournier")
  , ("Von Hirsch",    "von Hirsch")
  ]


{-|
  Takes a directory path and gets all entries of this directory path. For each
  entry that is also a directory path the function is executed recursively. Each
  entry that is a file path is added to the list of file paths (which is the
  global state).

  The function is equivalent to the (shell) command
  > find dirPath -type f
-}
readFilePaths :: FilePath -> StateT [FilePath] IO ()
readFilePaths dirPath = do
  entries <- liftIO $ listDirectory dirPath
  let entriesWithPath = (dirPath </>) <$> entries
  dirEntries <- liftIO $ filterM doesDirectoryExist entriesWithPath
  mapM_ readFilePaths dirEntries
  modify $ (<>) (entriesWithPath \\ dirEntries)


{-
  Takes a "from", a "to", and a "list" and replaces all "from" occurrences with
  "to" in "list". (Inspired by https://bluebones.net/2007/01/replace-in-haskell)

  >>> replace "a" "xy" "abcabc"
  "xybcxybc"
-}
replace :: Eq a => [a] -> [a] -> [a] -> [a]
replace [] _ list = list
replace _ _ [] = []
replace from to (x:xs)
  | from == fst splitPair = to <> replace from to (snd splitPair)
  | otherwise             = x  :  replace from to xs
  where
    splitPair = splitAt (length from) (x : xs)


{-
  Takes a list of String pairs and a String, creates a list of partially applied
  functions of type 'String -> String' (by mapping function 'replace') and folds
  the function composition operator over this list.

  The result is a function of type 'String -> String' that takes a String and
  replaces each string that matches the first pair element with the second pair
  element. Each pair of the list is applied one after the other.

  >>> apply replacements "Haelloe"
  "Hällö"
-}
apply :: [(String, String)] -> String -> String
apply sp = foldr1 (flip (.)) $ uncurry replace <$> sp


{-
  Takes a String and returns a String, where all words of the input String are
  capitalized.

  >>> capitalizeWords "HELLO"
  "Hello"

  >>> capitalizeWords "world"
  "World"
-}
capitalizeWords :: String -> String
capitalizeWords s = unwords $ capitalizeWord <$> words s
  where
    capitalizeWord :: String -> String
    capitalizeWord [] = ""
    capitalizeWord (x:xs) = toUpper x : (toLower <$> xs)


{-
  Takes a FilePath and returns a pair that consists of a String (the author
  name) as first element and a list of String (the title of the related book as
  a singleton list) as second element.

  The first two characters of the given file path (which are typically "./") are
  removed. In the given file path, author name and the book title are expected
  to be separated with '/'. The file extension (most likely '.epub' or '.pdf')
  is removed.

  >>> filePathToBookData "./AUTHOR/Series_of_Books/A_Book.epub"
  ("Author",["Series of Books - A Book"])
-}
filePathToBookData :: FilePath -> (String, [String])
filePathToBookData = hdlFields . splitOn "/" . adjustLine
  where
    adjustLine :: FilePath -> String
    adjustLine = apply replacements . drop 2 . dropExtension
    hdlFields :: [String] -> (String, [String])
    hdlFields [] = ("", [])
    hdlFields (x:xs) =
      (  apply corrections $ capitalizeWords x
      , [apply corrections $ intercalate " - " xs] )


{-
  Takes a list of pairs, where each pair consists of a String (the author name)
  as first element and a list of String (the titles of the related books as
  a list) as second element, and returns a 'BookDataMap' type, which is a map
  with a String (the author name) as key and a list of String (the titles of the
  related books) as value.

  >>> makeBookDataMap [("Author",["Books - A Book"]),("Author",["Book"])]
  fromList [("Author",["Book","Books - A Book"])]
-}
makeBookDataMap :: [(String, [String])] -> BookDataMap
makeBookDataMap = Map.fromListWith (<>)


{-|
  Takes a pair that consists of a String (the author name) as first element and
  a list of String (the titles of the related books as a list) as second element
  and outputs this data on 'stdout'.

  >>> putBookData ("Author",["Book","Books - A Book"])
  * Author
      * Book
      * Books - A Book
-}
putBookData :: (String, [String]) -> IO ()
putBookData (author, bookList) = do
  putStrLn $ "* " <> author
  putStrLn $ unlines $ ("    * " <>) <$> sort bookList


{-
  Takes a 'BookDataMap' type, which is a map with a String (the author name) as
  key and a list of String (the titles of the related books) as value, and
  outputs this data on 'stdout'.

  >>> putBookDataMap $ Map.fromList [("Author",["Book","Books - A Book"])]
  * Author
      * Book
      * Books - A Book
-}
putBookDataMap :: BookDataMap -> IO ()
putBookDataMap = mapM_ putBookData . Map.toAscList


{-|
  Takes a FilePath (the directory path 'DIR-PATH') and outputs book data on
  'stdout'.
-}
listBooks :: FilePath -> IO ()
listBooks dirPath = do
  setCurrentDirectory dirPath
  filePathList <- execStateT (readFilePaths ".") []
  putBookDataMap $ makeBookDataMap $ filePathToBookData <$> filePathList


{-|
  Takes  a list of String (the command line arguments) and a Maybe String (the
  possible value of the environment variable 'HOME') and returns a FilePath (the
  directory path 'DIR-PATH').

  If 'args' is not empty, the function uses the first element of 'args' as
  'DIR-PATH'. Otherwise, the function uses the concatenation of 'Just home' and
  'defaultDir'. If 'home' is 'Nothing' (that is, the environment variable 'HOME'
  is not set), the function uses 'defaultDir'.
-}
readDirPath :: [String] -> Maybe String -> FilePath
readDirPath args home = fromMaybe defaultDir (sHead args <|> mayAppend home)
  where
    sHead :: [String] -> Maybe FilePath
    sHead []    = Nothing
    sHead (x:_) = Just x
    mayAppend :: Maybe String -> Maybe FilePath
    mayAppend h = (</>) <$> h <*> pure defaultDir


{-|
  'main': Evaluates the command line arguments and the value of the environment
  variable 'HOME', reads 'DIR-PATH' (via function 'readDirPath'), and applies
  'listBooks', or outputs a usage, if '-h' is given.
-}
main :: IO ()
main = do
  let usage = "Usage: list-books [DIR-PATH]"
  args <- getArgs
  home <- lookupEnv "HOME"
  bool (listBooks $ readDirPath args home) (putStrLn usage) ("-h" `elem` args)


-- EOF
